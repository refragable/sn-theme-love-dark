This site is under construction.

# Love Dark Theme

Love Dark, a theme for [Standard Notes](https://standardnotes.org/), is based on the dark variant of the [Love theme](https://love.holllo.cc/).

# Installation

1. Open your Standard Notes desktop/web client
2. Click "Extensions" at the bottom left
3. Click "Import Extensions"
4. Paste [this link](https://refragable.gitlab.io/sn-theme-love-dark/love-dark.json) and press enter
5. Install the extension

# License

[AGPLv3](https://gitlab.com/refragable/sn-theme-love-dark/-/blob/master/LICENSE)

# Credits
Based on SN's [Autobiography theme](https://github.com/sn-extensions/autobiography-theme)
