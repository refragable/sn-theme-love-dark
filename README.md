# [Love Dark Theme](https://refragable.gitlab.io/sn-theme-love-dark/)

Love Dark, a theme for [Standard Notes](https://standardnotes.org/), is based on the dark variant of the [Love theme](https://love.holllo.cc/).

# Installation

1. Open your Standard Notes desktop/web client
2. Click "Extensions" at the bottom left
3. Click "Import Extensions"
4. Paste [this link](https://refragable.gitlab.io/sn-theme-dark-love/love-dark.json) and press enter
5. Install the extension

# Development

1. Clone the repo: `git clone https://gitlab.com/refragable/sn-theme-love-dark.git`
2. Go into the new directory: `cd sn-theme-love-dark`
3. Install dependencies: `npm`, `ruby`, `ruby-sass`
4. Install node packages: `npm install`
5. Edit the source file: `src/main.scss`
6. Build the theme for distribution: `npx grunt`
7. Test the newly built theme locally. Standard Notes has [a guide for this](https://docs.standardnotes.org/extensions/local-setup).


# License

[AGPLv3](https://gitlab.com/refragable/sn-theme-love-dark/-/blob/master/LICENSE)

# Credits
Based on SN's [Autobiography theme](https://github.com/sn-extensions/autobiography-theme)
