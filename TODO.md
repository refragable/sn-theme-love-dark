# High priority

[] Figure out why highlights aren't working

[X] The theme is not being applied correctly:
  > Refused to apply style from 'https://gitlab.com/refragable/sn-theme-love-dark/-/raw/master/dist/dist.css' because its MIME type ('text/plain') is not a supported stylesheet MIME type, and strict MIME checking is enabled.

# Medium priority

[X] Distribute theme via GitLab pages
  [X] Host css
  [X] Pretty install link
  [X] Update listed.to json file
  * Links: 
    * [SN Docs Publishing guide](https://docs.standardnotes.org/extensions/publishing/)
    * [SN Docs Themes guide](https://docs.standardnotes.org/extensions/themes)

[] Make the theme prettier

# Low priority

[] Make prettier docs page
  [] styling
  [] images
    [] screenshots


